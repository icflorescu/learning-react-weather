const { resolve }       = require('path'),
      webpack           = require('webpack'),
      autoprefixer      = require('autoprefixer'),
      ExtractTextPlugin = require('extract-text-webpack-plugin'),
      HtmlWebpackPlugin = require('html-webpack-plugin');

const plugins = [
  new HtmlWebpackPlugin({
    template: 'index.html',
    minify: { html5: true, collapseWhitespace: true }
  }),
  new webpack.LoaderOptionsPlugin({
    options: {
      sassLoader: { includePaths: [resolve(__dirname, './node_modules')] },
      postcss: [autoprefixer({ browsers: ['> 5%'] })],
      context: '/'
    }
  })
];

module.exports = env => {
  const dev = env ? env.dev : false;
  return {
    output: { path: resolve(__dirname, 'dist'), filename: 'index.min.js', publicPath: '/' },
    context: resolve(__dirname, 'src'),
    devtool: dev ? 'inline-source-map' : 'hidden-source-map',
    devServer: { hot: true, contentBase: '/dist', publicPath: '/' },

    entry: dev ? ['react-hot-loader/patch', './index.js'] : './index.js',

    module: {
      rules: [{
        test: /\.js$/,
        loaders: [{
          loader: 'babel-loader',
          query: {
            presets: [['es2015', { modules: false }], 'stage-2', 'react'],
            plugins: dev ? ['react-hot-loader/babel', 'react-html-attrs'] : ['react-html-attrs']
          }
        }],
        exclude: /node_modules/
      }, {
        test: /\.scss$/,
        loaders: dev ? [
          { loader: 'style-loader' },
          { loader: 'css-loader', query: { sourceMap: true } },
          { loader: 'postcss-loader', query: { sourceMap: true } },
          { loader: 'sass-loader', query: { sourceMap: true } },
        ] : ExtractTextPlugin.extract([
          { loader: 'css-loader', query: { sourceMap: true, minimize: true, discardComments: { removeAll: true } } },
          { loader: 'postcss-loader', query: { sourceMap: true } },
          { loader: 'sass-loader', query: { sourceMap: true } },
        ])
      }]
    },

    plugins: dev ? plugins.concat([
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NamedModulesPlugin(),
    ]) : plugins.concat([
      new webpack.optimize.DedupePlugin(),
      new webpack.DefinePlugin({
        'process.env': { NODE_ENV: JSON.stringify('production') }
      }),
      new webpack.optimize.UglifyJsPlugin({
        compress: { warnings: false },
        output: { comments: false },
        sourceMap: true
      }),
      new ExtractTextPlugin('index.min.css'),
    ])
  };
};
